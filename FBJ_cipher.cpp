#include <iostream>
#include <string>
#include <bitset>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <ctype.h>
using namespace std;

string Gencrypted;


string encrypt(string text, int shifts, char direction) {
    string alphabet = "abcdefghijklmnopqrstuvwxyz";   //string variable to hold alphabet
    string special= "!@#$%^&*().,?+-";
    string encrypted = "";
    int overload;   //variable that is used to help cipher return to beginning or end of alphabet if end is reached

    if((char) toupper(direction) == 'R') { // direction = right
        for(int i = 0; i < text.length(); i++) { //loop through text input
            for(int j = 0; j < alphabet.length(); j++) {  //for each char in text input, loop through alphabet
             if((char) toupper(text[i]) == (char) toupper(alphabet[j])) {  //if there is a match...
                    if((j + shifts) > (alphabet.length() - 1) ){  //if number of shifts goes past 'Z'...
                        overload = (j + shifts) - alphabet.length();  //go back to 'A'
                        //cout << (char) toupper(alphabet[overload]);
                        encrypted += (char) toupper(alphabet[overload]);
                    } else {
                        //cout << (char) toupper(alphabet[j + shifts]);
                        encrypted += (char) toupper(alphabet[j + shifts]);
                    }
                } else if(isdigit(text[i]) == 1 || special.find(text[i])!= -1) {
                    encrypted += text[i];
                    break;
                } else if(isalpha(text[i]) == 0 && isdigit(text[i]) == 0 ) {
                    encrypted += 'x';
                    break;
                }
            }
        }
    } else if((char) toupper(direction) == 'L') {  //direction = left
        for(int i = 0; i < text.length(); i++) {
            for(int j = 0; j < alphabet.length(); j++) {
                 if((char) toupper(text[i]) == (char) toupper(alphabet[j])) {
                    if((j - shifts < 0)) {  //if the number of shifts goes past 'A'
                        overload = (j - shifts) + alphabet.length();  //go back to 'Z'
                        encrypted += (char) toupper(alphabet[overload]);
                        //cout << (char) toupper(alphabet[overload]);
                    } else {
                        //cout << (char) toupper(alphabet[j - shifts]);
                        encrypted += (char) toupper(alphabet[j - shifts]);
                    }
                } else if(isdigit(text[i]) == 1 || special.find(text[i])!= -1) {
                    encrypted += text[i];
                    break;
                } else if(isalpha(text[i]) == 0 && isdigit(text[i]) == 0 ) {
                    encrypted += 'x';
                    break;
                }
            }
        }
    }

    string alphabet_1 = "abcdefghijklm01234!@#$%.";
    string alphabet_2 = "nopqrstuvwxyz56789^&*(),";


    for(int i = 0; i < encrypted.length(); i++) {
        for(int j = 0; j < alphabet_1.length(); j++) {
            if(encrypted[i] == (char) toupper(alphabet_1[j])) {
                encrypted[i] = (char) toupper(alphabet_2[j]);
            } else if(encrypted[i] == (char) toupper(alphabet_2[j])) {
                encrypted[i] = (char) toupper(alphabet_1[j]);
            }
        }

    }
    //cout << "Encrypted text: " << encrypted;
    Gencrypted = encrypted;
    return encrypted;
    }

    string decrypt(string encrypted, int shifts, char direction)
     {
        string alphabet_2 = "abcdefghijklm01234!@#$%.";
        string alphabet_1 = "nopqrstuvwxyz56789^&*(),";
        string special= "!@#$%^&*().,?+-";
        for(int i = 0; i < encrypted.length(); i++) {
            for(int j = 0; j < alphabet_1.length(); j++) {
                if(encrypted[i] == (char) toupper(alphabet_1[j])){
                    encrypted[i] = (char) toupper(alphabet_2[j]);
                } else if(encrypted[i] == (char) toupper(alphabet_2[j])){
                    encrypted[i] = (char) toupper(alphabet_1[j]);
                }
            }
        }

        string alphabet = "abcdefghijklmnopqrstuvwxyz";   //string variable to hold alphabet
        string decrypted = "";
        int overload;   //variable that is used to help cipher return to beginning or end of alphabet if end is reached
        string text;
        text = encrypted;
        if((char) toupper(direction) == 'R') { // direction = right
            for(int i = 0; i < text.length(); i++) { //loop through text input
                for(int j = 0; j < alphabet.length(); j++) {  //for each char in text input, loop through alphabet
                    if (text[i] == 'x' && i <text.length())
                    {
                        decrypted+=' ';
                        i++;
                    }
                 if((char) toupper(text[i]) == (char) toupper(alphabet[j])) {  //if there is a match...
                        if((j + shifts) > (alphabet.length() - 1) ){  //if number of shifts goes past 'Z'...
                            overload = (j + shifts) - alphabet.length();  //go back to 'A'
                            //cout << (char) toupper(alphabet[overload]);
                            decrypted += (char) toupper(alphabet[overload]);
                        } else {
                            //cout << (char) toupper(alphabet[j + shifts]);
                            decrypted += (char) toupper(alphabet[j + shifts]);
                        }
                    } else if(isdigit(text[i]) == 1 || special.find(text[i])!= -1) {
                        decrypted += text[i];   //adds int or special character to string
                        break;
                    } else if(isalpha(text[i]) == 0 ) {
                        decrypted += 'x';   //replaces spaces with x
                        break;
                    }
                }
            }
        } else if((char) toupper(direction) == 'L') {  //direction = left
            for(int i = 0; i < text.length(); i++) {
                for(int j = 0; j < alphabet.length(); j++) {
                    if (text[i] == 'x' && i <text.length())
                    {
                        decrypted+=' '; //replaces x with spaces
                        i++;
                    }
                     if((char) toupper(text[i]) == (char) toupper(alphabet[j])) {
                        if((j - shifts < 0)) {  //if the number of shifts goes past 'A'
                            overload = (j - shifts) + alphabet.length();  //go back to 'Z'
                            decrypted += (char) toupper(alphabet[overload]);
                            //cout << (char) toupper(alphabet[overload]);
                        } else {
                            //cout << (char) toupper(alphabet[j - shifts]);
                            decrypted += (char) toupper(alphabet[j - shifts]);
                        }
                    } else if(isdigit(text[i]) == 1|| special.find(text[i])!= -1) {
                        decrypted += text[i];
                        break;
                    } else if(isalpha(text[i]) == 0) {
                        decrypted += 'x';
                        break;
                    }
                }
            }
        }

        return decrypted;
    }

void begin()
{
    int shifts;   //variable to hold number of spaces in alphabet to shift
    char direction;   //variable to verify direction of shift
    string text;   //text to be encrypted
    string response;
    cout<<"Would you like to encrypt or decrypt?"<<endl;
    cin>>response;
    cin.get();

    if (response == "encrypt" || response == "Encrypt")
    {
        cout << "Enter the text to encrypt:" << endl;

        getline(cin, text);

        cout << "How many shifts? (Must be less than 26 and greater than 0)" << endl;
        cin >> shifts;

        if(shifts > 25 || shifts < 1)
        {
            cout << "Sorry, this is not an acceptable input";
        }
        else {

            cout << "Which direction? (Enter 'L' for left or 'R' for right)" << endl;
            cin >> direction;

                cout << "Original Text: " << text << endl;

                cout << "Encrypted text: " << encrypt(text, shifts, direction) << endl;
            }
        char r;
        cout << "Enter 'Y' to Restart or 'N' to end"<< endl;
        cin>>r;
        if(toupper(r) == 'Y')
        {
            begin();
        }
        else if (toupper(r) == 'N')
        {
            cout<<"The End"<<endl;
        }
    }
    else if (response == "decrypt" || response == "Decrypt" )
    {
        cout << "Enter the text to decrypt:" << endl;
        getline(cin, text);

        cout << "How many shifts? (Must be less than 26 and greater than 0)" << endl;
        cin >> shifts;

        if(shifts > 25 || shifts < 1)
        {
            cout << "Sorry, this is not an acceptable input";
        } else {

            cout << "Which direction? (Enter 'L' for left or 'R' for right)" << endl;
            cin >> direction;


                cout << "Original Text: " << text << endl;
                cout << "Decrypted text: " << decrypt(text,shifts,direction) << endl;
    }
    char r;
    cout << "Enter 'Y' to Restart or 'N' to end"<< endl;
    cin>>r;
    if(toupper(r) == 'Y')
    {
        begin();
    }
    else if (toupper(r) == 'N')
    {
        cout<<"The End"<<endl;
    }
    }
    else
    {
    cout<< "Invalid entry."<<endl;
    begin();
    }
}
int main()
{
    //display to user the following
    begin();
    return 0;
}
